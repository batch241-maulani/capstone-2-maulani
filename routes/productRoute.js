const express = require('express')


const router = express.Router()
const productController = require("../controllers/productController")

const auth = require("../auth")


router.post("/create",auth.verify, (req,res) => {

	const userData = auth.decode(req.headers.authorization)

	productController.createProduct(req.body,{isAdmin: userData.isAdmin}).then(resultFromController=> res.send(resultFromController))
})

router.get("/activeproducts", (req,res)=> {
	productController.getAllActiveProducts().then(resultFromController=> res.send(resultFromController))
})

router.get("/:productId", (req,res)=>{
	productController.getProduct(req.params).then(resultFromController => res.send(resultFromController))
})

router.put("/:productId",auth.verify, (req,res)=>{
	const userData = auth.decode(req.headers.authorization)
	productController.updateProduct(req.params,req.body,{isAdmin: userData.isAdmin}).then(resultFromController=> res.send(resultFromController))
})

router.patch("/:productId/archive",auth.verify, (req,res)=>{
	const userData = auth.decode(req.headers.authorization)
	productController.archiveProduct(req.params,{isAdmin: userData.isAdmin}).then(resultFromController => res.send(resultFromController))
})

router.post("/retrieveallproducts",auth.verify,(req,res)=>{
	let data = {
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	}
	productController.getAllProducts(data).then(resultFromController=> res.send(resultFromController))
})

module.exports = router





