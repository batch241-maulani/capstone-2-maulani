const express = require('express')


const router = express.Router()
const userController = require("../controllers/userController")

const auth = require("../auth")



router.post("/register",(req,res)=>{

	userController.registerUser(req.body).then(resultFromController => res.send(resultFromController))


})


router.post("/login",(req,res)=>{
	userController.loginUser(req.body).then(resultFromController=>res.send(resultFromController))
})


router.post("/order", auth.verify,(req,res)=>{
	let data = {
		userId: auth.decode(req.headers.authorization).id,
		productId: req.body.productId,
		quantity: req.body.quantity,
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	}

	userController.order(data).then(resultFromController=> res.send(resultFromController))

})
router.get("/details", auth.verify, (req, res) => {
	const userData = auth.decode(req.headers.authorization);
		console.log(userData)
		console.log(req.headers.authorization);
	userController.getProfile({id: userData.id}).then(resultFromController => res.send(resultFromController))
});



router.get("/:userId",(req,res)=>{
	userController.getUser(req.params).then(resultFromController => res.send(resultFromController))
})


router.post("/addToCart", auth.verify,(req,res)=>{
	let data = {
		userId: auth.decode(req.headers.authorization).id,
		productId: req.body.productId,
		quantity: req.body.quantity,
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	}

	userController.addToCart(data).then(resultFromController => res.send(resultFromController))

})

router.post("/checkout", auth.verify, (req,res)=> {
	let data = {
		userId: auth.decode(req.headers.authorization).id,
	}

	userController.checkOutCart(data).then(resultFromController => res.send(resultFromController))

})

router.patch("/deleteitem",auth.verify,(req,res)=> {
	let data = {
		userId: auth.decode(req.headers.authorization).id,
		itemId: req.body.itemId
	}

	userController.deleteItemFromCart(data).then(resultFromController => res.send(resultFromController))

})

router.patch("/updatequantity", auth.verify,(req,res)=>{
	let data = {
		userId: auth.decode(req.headers.authorization).id,
		itemId: req.body.itemId,
		quantity: req.body.quantity
	}

	userController.updateQuantityInCart(data).then(resultFromController => res.send(resultFromController))

})

router.patch("/makeadmin", auth.verify,(req,res)=>{
	let data = {
		isAdmin: auth.decode(req.headers.authorization).isAdmin,
		userId: req.body.userId
	}

	userController.makeAdmin(data).then(resultFromController => res.send(resultFromController))



})

router.post("/getorder",auth.verify,(req,res)=>{

	let data = {
		userId: auth.decode(req.headers.authorization).id
	}
	userController.getOrders(data).then(resultFromController=> res.send(resultFromController))



})

router.post("/getallorder",auth.verify,(req,res)=>{

	let data = {
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	}
	userController.getAllOrders(data).then(resultFromController=> res.send(resultFromController))

})

module.exports = router


