const mongoose = require("mongoose")


const userSchema = mongoose.Schema({
	firstName: {
		type: String,
		required: [true, "First Name is required"]
	},
	lastName: {
		type: String,
		required: [true, "Last Name is required"]
	},
	email: {
		type: String,
		required: [true, "Email is required"]
	},
	password: {
		type: String,
		required: [true, "Password is required"]
	},
	isAdmin: {
		type: Boolean,
		default: false
	},
	cart:{
		items: [{
			productId: {
				type: String,
				required: [true, "Product id is required"]
			},
			productName: {
				type: String,
				required: [true, "Product name is required"]
			},
			quantity: {
				type: Number,
				required: [true, "Quantity is required"]
			}
			,
			subTotal: {
				type: Number,
				required: [true, "Sub Total is required"]
			},
			purchasedOn: {
				type: Date,
				default: new Date()
			}
		}],
		totalAmount: {
			type: Number,
			default: 0
		}

	},
	orders: [{
		products: [{
			productId: {
				type: String,
				required: [true, "Product id is required"]
			},
			productName: {
				type: String,
				required: [true, "Product name is required"]
			},
			quantity: {
				type: Number,
				required: [true, "Quantity is required"]
			},
			subTotal: {
				type: Number,
				required: [true, "Quantity is required"]
			}
		}],
		totalAmount: {
			type: Number,
			required: [true, "Total Amount is required"]
		},
		purchasedOn: {
			type: Date,
			default: new Date()
		}
	}]





})


module.exports = mongoose.model("User",userSchema)
