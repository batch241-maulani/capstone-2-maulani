const User = require("../models/User")
const Product = require("../models/Product")

const Order = require("../models/Order")


const bcrypt = require("bcrypt")

const auth = require("../auth")



module.exports.registerUser = async (reqBody)=>{

	let isUserExist = await User.findOne({email: reqBody.email}).then(result =>{
		if(result == null){
			return false
		}else{
			return true
		}
	})
	
	

	if(!isUserExist){

		let newUser = new User({
			firstName: reqBody.firstName,
			lastName: reqBody.lastName,
			email: reqBody.email,
			password: bcrypt.hashSync(reqBody.password,10)

		})

		return newUser.save().then((user, error)=>{
			if(error){
				console.log(error)
				return false
			}else {
				return user
			}
		})
	}
	let message = Promise.resolve("User already exist!")
	return message.then((value)=>{
		return {value}
	})
	
}



module.exports.loginUser = (reqBody) => {


	return User.findOne({email: reqBody.email}).then(result => {
		if(result == null){
			return false
		}else{
			const isPasswordCorrect = bcrypt.compareSync(reqBody.password,result.password)

			if(isPasswordCorrect){
				console.log(result)
				return {

					access: auth.createAccessToken(result)
				}
			}else {
				return false
			}

		}
	})


}


module.exports.order = async (data) => {
	if(data.isAdmin){
		let message = Promise.resolve("The User is Admin!")
		return message.then((value)=>{
			return {value}
		})
	}


	let totalAmounts = await Product.findById(data.productId).then(product=>{

		return product.price * data.quantity


	})

	let getProductInfo = await Product.findById(data.productId).then(product=>{
		return product
	})


	let orderData = new Order({
		userId: data.userId,
		products: [{
			productId: data.productId,
			productName: getProductInfo.name,
			quantity: data.quantity
		}],
		totalAmount: totalAmounts
	})

	let isOrderAdded = await orderData.save().then((order,error)=>{
		if(error){
			return false
		}else{
			return order._id
		}


	})


	let isUserUpdated = await User.findById(data.userId).then(user =>{
		user.orders.push({ 
			products: [{
				productId: data.productId,
				productName: getProductInfo.name,
				quantity: data.quantity,
				subTotal: totalAmounts
			}],
			totalAmount: totalAmounts
		})
		return user.save().then((user,error)=>{
			if(error){
				return false
			}else{
				return true
			}
		})
	})

	let isProductUpdated = await Product.findById(data.productId).then(product=>{
		product.stock = product.stock - data.quantity
		product.orders.push({
			orderId: isOrderAdded,
			userId: data.userId,
			quantity: data.quantity,
			totalAmount: totalAmounts
		})
		return product.save().then((product,error)=> {
			if(error){
				return false
			}else{
				return true
			}

		})
	})



	let message = Promise.resolve(isUserUpdated)
	return message.then((value)=>{
		return {value}
	})
	

}

module.exports.addToCart = async (data) => {
	if(data.isAdmin){
		let message = Promise.resolve("The User is Admin!")
		return message.then((value)=>{
			return {value}
		})
	}

	let totalAmounts = await Product.findById(data.productId).then(product=>{

		return product.price * data.quantity


	})

	let getProductInfo = await Product.findById(data.productId).then(product=>{
		return product
	})

	let isUserUpdated = await User.findById(data.userId).then(user =>{
		user.cart.items.push({ 
			productId: data.productId,
			productName: getProductInfo.name,
			quantity: data.quantity,
			subTotal: totalAmounts
		})

		let totalItemAmount = 0
		user.cart.items.forEach(item => {

			totalItemAmount += item.subTotal

		})

		user.cart.totalAmount = totalItemAmount

		return user.save().then((user,error)=>{
			if(error){
				return false
			}else{
				return true
			}
		})
	})
	let message = Promise.resolve('Added')
	return message.then((value)=>{
		return {value}
	})






}


module.exports.checkOutCart = async (data) => {

	if(data.isAdmin){
		let message = Promise.resolve("The User is Admin!")
		return message.then((value)=>{
			return {value}
		})
	}

	let isCartEmpty = await User.findById(data.userId).then(user=>{
		if(user.cart.totalAmount == 0 && user.cart.items.length == 0){
			return true
		}else {
			return false
		}
	})

	if(!isCartEmpty){

		let totalAmounts = await User.findById(data.userId).then(user=>{

			return user.cart.totalAmount


		})

		let getProductIds = await User.findById(data.userId).then(product=>{
			let ids = [] 
			product.cart.items.forEach(pr => {
				console.log(pr._id)
				ids.push(pr._id)
			})

			return ids
		})

		console.log(getProductIds)




		let items = []
		let isUserUpdated = await User.findById(data.userId).then(user =>{

			user.cart.items.forEach(pr=>{
				console.log(pr)
				items.push(pr)

			})

			user.orders.push({
				products: [{
					productId: items[0].productId,
					productName: items[0].productName,
					quantity: items[0].quantity,
					subTotal: items[0].subTotal
				}],
				totalAmount: totalAmounts


			})

			items.forEach(elem=>{
				if(items.indexOf(elem)==0){
					return
				}else{
					user.orders[user.orders.length-1].products.push({
						productId: elem.productId,
						productName: elem.productName,
						quantity: elem.quantity,
						subTotal: elem.subTotal
					})
				}
			})


			console.log(items)

			user.orders.totalAmount = totalAmounts

			user.cart.totalAmount = 0 
			user.cart.items = []

			return user.save().then((user,error)=>{
				if(error){
					return false
				}else{
					return true
				}
			})
		})


		let orderData = new Order({
			userId: data.userId,
			totalAmount: totalAmounts
		})

		items.forEach(elem => {
			orderData.products.push({
				productId: elem.productId,
				productName: elem.productName,
				quantity: elem.quantity,
				subTotal: elem.subTotal
			})
		})
		let isOrderAdded = await orderData.save().then((order,error)=>{
			if(error){
				return false
			}else{
				return order._id
			}


		})



		items.forEach(elem=>{

			let isProductUpdated = Product.findById(elem.productId).then(product=>{
				product.stock = product.stock - elem.quantity
				product.orders.push({
					orderId: isOrderAdded,
					userId: data.userId,
					quantity: elem.quantity,
					totalAmount: elem.subTotal
				})
				return product.save().then((product,error)=>{
					if(error){
						return false
					}else{
						return true
					}
				})

			})


		})
	}else{
		let message = Promise.resolve("The Cart is Empty!")
		return message.then((value)=>{
			return {value}
		})
	}

	let message = Promise.resolve("Ordered!")
		return message.then((value)=>{
			return {value}
		})
	
}


module.exports.deleteItemFromCart = async (data) => {

	let isCartEmpty = await User.findById(data.userId).then(user=>{
		if(user.cart.totalAmount == 0 && user.cart.items.length == 0){
			return true
		}else {
			return false
		}
	})

	if(!isCartEmpty){

		let deleteItem = await User.findById(data.userId).then(user=>{
			user.cart.items.splice(user.cart.items.indexOf({_id: data.itemId}),1)

			let newTotalAmount = 0

			user.cart.items.forEach(elem => {
				newTotalAmount += elem.subTotal
			})

			user.cart.totalAmount = newTotalAmount


			return user.save().then((user,error)=>{
				if(error){
					return false
				}else{
					return true
				}
			})
		})



	}else{
		let message = Promise.resolve("The Cart is Empty!")
		return message.then((value)=>{
			return {value}
		})
	}
	let message = Promise.resolve("Item Deleted")
		return message.then((value)=>{
			return {value}
		})

}


module.exports.updateQuantityInCart = async (data) =>{

	let isCartEmpty = await User.findById(data.userId).then(user=>{
		if(user.cart.totalAmount == 0 && user.cart.items.length == 0){
			return true
		}else {
			return false
		}
	})

	if(!isCartEmpty){
		let productId = ""

		let getProductId = await User.findById(data.userId).then(user=> {

			let indexOfItem = 0
			user.cart.items.forEach(elem=>{

				if(elem._id.toString() == data.itemId){
					
					indexOfItem = user.cart.items.indexOf(elem)
				}


			})
			productId = user.cart.items[indexOfItem].productId	

			return true
		})

		let getPrice = await Product.findById(productId).then(product=>{

			return product.price


		})

		let updateTotal = await User.findById(data.userId).then(user =>{
			let indexOfItem = 0
			user.cart.items.forEach(elems => {
				if(elems._id.toString() == data.itemId){
					
					indexOfItem = user.cart.items.indexOf(elem)
				}
			})
			user.cart.items[indexOfItem].quantity = data.quantity
			user.cart.items[indexOfItem].subTotal = getPrice * data.quantity
			let newTotalAmount = 0

			user.cart.items.forEach(elem => {
				newTotalAmount += elem.subTotal
			})

			user.cart.totalAmount = newTotalAmount

			return user.save().then((user,error)=>{
				if(error){
					return false
				}else{
					return true
				}
			})

		})

	}else{
		let message = Promise.resolve("The Cart is Empty!")
		return message.then((value)=>{
			return {value}
		})
	}
	let message = Promise.resolve("Updated")
		return message.then((value)=>{
			return {value}
		})


}

module.exports.getProfile = (userData) => {
	return User.findById(userData.id).then(result => {
		// console.log(data.userId);
		// console.log(result);
		
		if (result == null) {
			return false
		} else {
			result.password = "*****"

			// Returns the user information with the password as an empty string or asterisk.
			return result
		}
	})
};

module.exports.getUser = (reqParams) =>{

	return User.findById(reqParams.userId).then((user,error)=>{
		if(error){
			return false
		}else{
			return user
		}
	})

}


module.exports.makeAdmin = (data) => {

	if(data.isAdmin){
		let makeUserAdmin = User.findByIdAndUpdate(data.userId,{isAdmin:true}).then((user,error)=>{
			if(error){
				return false
			}else {
				return true
			}
		})
		let message = Promise.resolve("Succesfully Updated")
		return message.then((value)=>{
			return {value}
		})
	}
	else{
		let message = Promise.resolve("User is not an admin!")
		return message.then((value)=>{
			return {value}
		})
	}

}

module.exports.getOrders = (data) => {

	console.log(data.userId)

	return User.findById(data.userId).then(user=>{
		return user.orders
	})

	let message = Promise.resolve("User is not an admin!")
	return message.then((value)=>{
		return {value}
	})
}

module.exports.getAllOrders = (data) => {

	if(data.isAdmin){
		return Order.find({}).then(result=>{
			return result
		})
	}
	else{
		let message = Promise.resolve("User is not an admin!")
		return message.then((value)=>{
			return {value}
		})
	}
}
