const Product = require("../models/Product")




const bcrypt = require("bcrypt")

const auth = require("../auth")


module.exports.createProduct = (reqBody, isAdmin) => {



	if(isAdmin.isAdmin){
		let newProduct = new Product({
			name: reqBody.name,
			description: reqBody.description,
			stock: reqBody.stock,
			imagelink: reqBody.imagelink,
			price: reqBody.price
		})
		return newProduct.save().then((product,error)=>{
			if(error){
				return false
			}else{
				return true
			}
		})
	}

	let message = Promise.resolve("User is not an admin!")
	return message.then((value)=>{
		return {value}
	})

}


module.exports.getAllActiveProducts = () =>{
	return Product.find({isActive: true}).then(result=>{
		return result
	})
}

module.exports.getProduct = (reqParams)=>{
	return Product.findById(reqParams.productId).then(result=>{
		return result
	})
}



module.exports.updateProduct = (reqParams,reqBody,isAdmin) => {
	if(isAdmin.isAdmin)
	{
		let updateProduct = {
			name: reqBody.name,
			description: reqBody.description,
			price: reqBody.price,
			stock: reqBody.stock,
			imagelink: reqBody.imagelink,
			isActive: reqBody.isActive
		}

		return Product.findByIdAndUpdate(reqParams.productId,updateProduct).then((product,error)=>{
			if(error){
				return false
			}else{
				return true
			}
		})
	}

	let message = Promise.resolve("User is not an admin!")
	return message.then((value)=>{
		return {value}
	})


}

module.exports.archiveProduct = (reqParams,isAdmin) =>{
	if(isAdmin.isAdmin)
	{
		let archiveProduct = {
			isActive: false
		}

		return Product.findByIdAndUpdate(reqParams.productId,archiveProduct).then((product,error)=>{
			if(error){
				return false
			}else{
				return true
			}
		})
	}

	let message = Promise.resolve("User is not an admin!")
	return message.then((value)=>{
		return {value}
	})
}



module.exports.getAllProducts = (data) => {
	if(data.isAdmin){
		return Product.find({}).then(result=>{
			return result
		})
	}
	else{
		let message = Promise.resolve("User is not an admin!")
		return message.then((value)=>{
			return {value}
		})
	}
	let message = Promise.resolve("Retrieved!")
	return message.then((value)=>{
		return {value}
	})
}
